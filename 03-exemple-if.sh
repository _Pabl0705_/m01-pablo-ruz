#! /bin/bash
# @edt ASIX-M01
# 16 de Febrer 2023

# Exemple dels if: indicar si es major d'edat
#  $ prog edat
# ----------------------------------------------------

#1) Validar si hem rebut un argument

if [ $# -ne 1 ]
then 
  echo "Error: número args incorrecte"
  echo "Usage: $0 edat"
  exit 1
fi

#2) comprovar si és menor d'edat, població activa o jubilació
edat=$1
if [ $edat -lt 18 ]
then
  echo "edat $edat és menor d'edat"
elif [ $edat -lt 65 ]
then
  echo "edat $edat és troba a la població activa"
else
  echo "edat $edat és edat de jubilació"
fi
exit 0
