#! /bin/bash
# @edt ASIX-M01
# 27 de Febrer 2023
# Pablo Ruz Muñoz
# Programa que mostra el nom i l'edat introduida
# Utilització: prog nom edat
# ----------------------------------------------------------------------

#1) Validació del nombre d'arguments

if [ $# -ne 2 ]
then
  echo "ERROR: Número d'arguments incorrectes"
  echo "USAGE: $0 nom edat"
  exit 1
fi

#2) xixa

echo "nom: $1"
echo "edat: $2"
exit 0
