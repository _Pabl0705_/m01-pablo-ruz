#! /bin/bash
# @edt ASIX-M01
# 27 de Febrer 2023
# Pablo Ruz Muñoz
# Programa mostra si la nota introduida està aprovada o suspesa
# Utilització: prog nota
# ----------------------------------------------------------------------

#1) Validació del nombre d'arguments

if [ $# -ne 1 ]
then
  echo "ERROR: Número d'arguments incorrecte"
  echo "USAGE: $0 nota"
  exit 1
fi

#2) Validació nota

if [ $1 -gt 10 -o $1 -lt 0 ]
then
  echo "ERROR: El valor no és vàlid"
  echo "Nota és un valor entre el 0-10"
  echo "USAGE: $0 nota"
  exit 2
fi

#3) Comprobació nota suspesa o aprovada
nota=$1
if [ $nota -ge 5 ]
then
  echo "La nota $nota està aprovada"
else
  echo "La nota $nota està suspesa"
fi
exit 0


