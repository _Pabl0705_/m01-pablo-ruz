#! /bin/bash
# @edt ASIX-M01
# 27 de Febrer 2023
# Pablo Ruz Muñoz
# Programa que segons la nota determina si és un suspès, suficient, notable o excel·lent
# Utilització: prog nota
# ----------------------------------------------------------------------

#1) Validació del nombre d'arguments

if [ $# -ne 1 ]
then
  echo "ERROR: Número d'arguments incorrecte"
  echo "USAGE: $0 nota"
  exit 1
fi

#2) Validació nota

if [ $1 -gt 10 -o $1 -lt 0 ]
then
  echo "ERROR: El valor no és vàlid"
  echo "Nota és un valor entre el 0-10"
  echo "USAGE: $0 nota"
  exit 2
fi

#3) Comprobació rang nota
nota=$1
if [ $nota -lt 5 ]
then
  echo "La nota $nota està suspesa"
elif [ $nota -lt 7 ]
then
  echo "La nota $nota és suficient"
elif [ $nota -lt 9 ]
then
  echo "La nota $nota és notable"
else
  echo "La nota $nota és excel·lent"
fi
exit 0
