#! /bin/bash
# @edt ASIX-M01
# 27 de Febrer 2023
# Pablo Ruz Muñoz
# Programa que mostra la llista del directori introduit
# Utilització: prog dir
# ----------------------------------------------------------------------

#1) Validació del nombre d'arguments

if [ $# -ne 1 ]
then
  echo "ERROR: Número d'arguments incorrecte"
  echo "USAGE: $0 dir"
  exit 1
fi

#2) Comprobació si és un directori

if [ -f $1 ]
then
  echo "ERROR: L'argument introduit no és un directori"
  echo "USAGE: $0 dir"
  exit 2
fi

#3) Llistat del directori indicat

dir=$1
ls $dir
