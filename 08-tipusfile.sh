#! /bin/bash
# @edt ASIX-M01
# 16 de Febrer 2023
# Pablo Ruz Muñoz
# Programa que mostra el tipus de file 
# Utilització: prog file
# ----------------------------------------------------------------------

#1) Validació del nombre d'arguments

if [ $# -ne 1 ]
then
  echo "ERROR: Número d'arguments incorrecte"
  echo "USAGE: $0 nota"
  exit 1
fi

#2) Determinar el tipus de file
file=$1
if [ ! -e $file ]; then
  echo "$file no existeix"
  exit 2
elif [ -f $file ]; then
  echo "$file és un regular file"
elif [ -d $file ]; then
  echo "$file és un directori"
elif [ -h $file ]; then
  echo "$file és un link"
else
  echo "$file és una altra cosa"
exit 0
