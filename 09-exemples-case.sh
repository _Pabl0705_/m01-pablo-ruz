#! /bin/bash
# @edt ASIX-M01
# 2 de març de 2023
# Pablo Ruz Muñoz
# Programa que segons el nom posa el seu genere 
# Utilització: prog nom
# ----------------------------------------------------------------------

# INICI PROGRAMA

case $1 in
  d[ltcjv])
    echo "És laborable"
    ;;
  d[sm])
    echo "És festiu"
    ;;
  *)
    echo "ERROR: El valor introduit és incorrecte"
    echo "USAGE: $0 dia de la setmana (format: dl)"
    exit 2 

exit 0

# INICI PROGRAMA

#1) Validació del nombre d'arguments

if [ $# -ne 1 ]; then
  echo "ERROR: El número d'arguments és incorrecte"
  echo "USAGE: $0 lletra"
  exit 1
fi

#2) Validació si és 1 lletra

case $1 in
  [aeiou])
    echo "És una vocal"
    ;;
  [bcdfghjklmnpqrstvywxz])
    echo "És una consonant"
    ;;
esac
exit 0

# INICI PROGRAMA

#1) Validació del nombre d'arguments

if [ $# != 1 ]; then
  echo "ERROR: El número d'arguments és incorrecte"
  echo "USAGE: $0 nom"
  exit 1
fi
#2) xixa
case $1 in
  "pere"|"marta"|"joan")
    echo "és un nen"
    ;;
  "marta"|"anna"|"julia")
    echo "és una nena"
    ;;
  *)
    echo "no binari"
    ;;
esac
exit 0
