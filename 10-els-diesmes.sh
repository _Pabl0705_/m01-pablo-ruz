#! /bin/bash
# @edt ASIX-M01
# 2 de març 2023
# Pablo Ruz Muñoz
# Programa mostra quants dies té el mes indicat 
# Utilització: prog mes
# ----------------------------------------------------------------------

#1) Validació del nombre d'arguments

if [ $# -ne 1 ]; then
  echo "ERROR: El número d'arguments és incorrecte"
  echo "USAGE: $0 número_del_mes"
  exit 1
fi

#2) Validació del numero del mes

mes=$1
if [ $mes -le 0 -a $mes -gt 12 ]; then
  echo "ERROR: El número introduit es troba fora del rang"
  echo "USAGE: $0 número_del_mes"
  echo "numero_del_més ha de ser un número entre 1 i 12"
  exit 2
fi

#3) Comprobació numero de dias segons el mes

case $mes in
  "1"|"3"|"5"|"7"|"8"|"10"|"12")
    echo "Aquest mes té 31 dies"
    ;;
  "2")
    echo "Aquest mes té 28 dies"
    ;;
  *)
    echo "Aquest mes té 30 dies"
    ;;
esac
exit 0
