#! /bin/bash
# @edt ASIX-M01
# 6 de març de 2023
# Pablo Ruz Muñoz
# Descripció: Exemple bucle for 
# ----------------------------------------------------------------------

#8) Llistar tots els logins numerats

llistat=$(cut -d: -f1 /etc/passwd)
contador=1

for login in $llistat
do
  echo "$contador: $login"
  ((contador++))
done
exit 0

#7) Numerar els fitxers del directori actiu

llistat=$(ls)
contador=1

for file in $llistat
do
  echo "$contador: $file"
  ((contador++))
done
exit 0

#6) Numerar els arguments

contador=1
for arg in $*
do
  echo "$contador: $arg"
  contador=$((contador + 1))
done
exit 0

#5) Iterar i mostrar la llista d'arguments

for argument in $@
do
  echo "$argument"
done
exit 0

#4) Iterar i mostrar la llista d'arguments

for argument in $*
do
  echo "$argument"
done
exit 0

#3) Iterar pel valor d'una variable

llistat=$(ls)
for nom in $llistat
do
  echo "$nom"
done
exit 0

#2) Iterar per un conjunt d'elements

for nom in "pere marta pau anna"
do
  echo "$nom"
done
exit 0

#1) Iterar per un conjunt d'elements

for nom in "pere" "marta" "pau" "anna"
do
  echo "$nom"
done
exit 0
