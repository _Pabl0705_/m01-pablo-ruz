#! /bin/bash
# @edt ASIX-M01
# 8 de març de 2023
# Pablo Ruz Muñoz
# Descripció: programa que suma els nombres introduits
# Utilització: prog num1...

# 1) Validar arguments

if [ $# -lt 1 ]
then
  echo "ERROR: El nombre d'arguments és incorrecte"
  echo "USAGE: #0 num1..."
  exit 1
fi

# 2) xixa

sum=0

for num in $*
do
  sum=$(($sum + $num))
done

echo "La suma és $sum"

exit 0



