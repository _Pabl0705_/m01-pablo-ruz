#! /bin/bash
# @edt ASIX-M01
# 8 de març de 2023
# Pablo Ruz Muñoz
# Descripció: Programa que llegeix notes i determina la qualificació
# Utilització: prog nota...

# 1) Validar arguments

if [ $# -lt 1 ];
then
  echo "ERROR: El nombre d'arguments és incorrecte"
  echo "USAGE: #0 nota1..."
  exit 1
fi

# 2) Comprovar si són notes vàlides

for nota in $*
do
  if [ $nota -gt 10 -o $nota -lt 0 ];
  then
    echo "ERROR: La nota $nota ha de estar entre el 0-10" >> /dev/stderr
  fi
done

# 3) xixa

for nota in $*
do
  if [ $nota -gt 10 -o $nota -lt 0 ];
  then
    echo "ERROR: La nota $nota no es troba en el rang. La nota ha de estar entre el 0-10" >> /dev/stderr
  elif [ $nota -lt 5 ];
  then
    echo "La nota $nota està suspesa"
  elif [ $nota -lt 7 ];
  then
    echo "La nota $nota és suficient"
  elif [ $nota -lt 9 ];
  then
    echo "La nota $nota és notable"
  else
    echo "La nota $nota és excel·lent"
  fi
done

exit 0






