#! /bin/bash
# @edt ASIX-M01
# 9 de març de 2023
# Pablo Ruz Muñoz
# Descripció: eexemples while

# 7) Numera i mostra en majuscules
#    l'entrada estàndard

comptador=1
while read -r line
do
  echo "$comptador: $line" | tr 'a-z' 'A-Z'
  ((comptador++))
done
exit 0

# 6) Mostrar stdin linia a linia fins
#    token FI

read -r line
while [ $line != "FI" ]
do
  echo "$line"
  read -r line
done
exit 0

# 5) Numerar stdin línea a línea
comptador=1
while read -r line
do
  echo "$comptador: $line"
  ((comptador++))
done
exit 0


# 4) Processar entrada estandard linia
#    a linia

while read -r line
do
  echo $line
done
exit 0

# 3) Iterar per la llista d'arguments

while [ -n "$1" ]
do
  echo "$1 $# $*"
  shift
done
exit 0

# 2) Comptador decreixent n(arg)...0

num=$1
fin_contador=0
while [ $num -ge $fin_contador ]
do
  echo "$num"
  ((num--))
done
exit 0

# 1) Validar arguments
num=1
fin_contador=10
while [ $num -le $fin_contador ]
do
  echo "$num"
  ((num++))
done

exit 0
