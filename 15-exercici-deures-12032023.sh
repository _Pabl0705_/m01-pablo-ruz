
ERR_ARGS=1
ERR_VALOR_ARG=2


# Exercici 10

fin_contador=$1
contador=1
while read -r line
do	
  if [ $contador -le $fin_contador ];then
    echo "$contador: $line"
    ((contador++))
  fi
done

# Exercici 9

while read -r line
do
  grep -q "^$line:" /etc/passwd
  if [ $? -eq 0 ]; then
    echo "$line" >> /dev/stdout
  else
    echo "$line" >> /dev/stderr
  fi
done
exit 0

# Exercici 8

for usuari in $*
do
  grep -q "^$usuari:" /etc/passwd
  if [ $? -eq 0 ]; then
    echo "$usuari" >> /dev/stdout
  else
    echo "$usuari" >> /dev/stderr
  fi
done
exit 0

# Exercici 7

# 1) xixa

while read -r line
do
  echo "$line" | grep -E "^..{60}"
done
exit 0

# Exercici 6

# 1) Validar número d'arguments

if [ $# -eq 0 ]
then
  echo "ERROR: Número d'arguments incorrecte"
  echo "USAGE: $0 número"
  exit $ERR_ARGS
fi

# 2) xixa

for dia_setmana in $* 
do
  case $dia_setmana in
    "Dilluns"|"Dimarts"|"Dimecres"|"Dijous"|"Divendres")
      echo "$dia_setmana: És un dia laborable"
      ;;
    "Dissabte"|"Diumenge")
      echo "$dia_setmana: És un dia festiu"
      ;;
    *)
      echo "ERROR: $dia_setmana no és un dia de la setmana" 2> stderr
      ;;
  esac
done
exit 0

# Exercici 5

while read -r line
do
  echo "$line" | cut -c1-50
done

# Exercici 4

# 1) Validar número d'arguments

if [ $# -lt 1 ]
then
  echo "ERROR: Número d'arguments incorrecte"
  echo "USAGE: $0 número"
  exit $ERR_ARGS
fi

# 2) xixa

for mes_rebut in $*
do
  if [ $mes_rebut -le 0 -o $mes_rebut -gt 12 ]
  then
    echo "ERROR: El mes $mes_rebut no es troba al rang"
    echo "USAGE: $0 mes..." 
    echo "Sent mes un número entre el 1 i el 12"
  else
    case $mes_rebut in
      "4"|"6"|"9"|"11")
        echo "El mes $mes_rebut té 30 dies"
        ;;
      "2")
        echo "El mes $mes_rebut té 28 dies"
        ;;
      *)
        echo "El mes $mes_rebut té 31 dies"
        ;;
    esac
  fi
done

exit 0

# Exercici 3

# 1) Validar número d'arguments

if [ $# -ne 1 ]
then
  echo "ERROR: Número d'arguments incorrecte"
  echo "USAGE: $0 número"
  exit $ERR_ARGS
fi

# 2) xixa

fi_contador=$1
contador=0

while [ $contador -le $fi_contador ]
do
  echo "$contador"
  ((contador++))
done

exit 0

# Exercici 2

# 1) xixa

comptador=1

for arg in $*
do
  echo "$comptador: $arg"
  ((comptador++))
done
exit 0

# Exercici 1

# 1) xixa

comptador=1

while read -r line
do
  echo "$comptador: $line"
  ((comptador++))
done
exit 0
