ERR_ARGS=1
ERR_TYPE_ARG=2

# Exercici 3

for matricula in $*
do
  echo $matricula | grep -qE "[0-9]{4}-[A-Z]{3}"

# Exercici 2

contador=0

for arg in $*
do
  echo $arg | grep -qE "^.{3}"
  if [ $? -eq 0 ] ; then
    ((contador++))
  fi
done
echo $contador
exit 0
  

# Exercici 1

for arg in $*
do
  echo $arg | grep -qE "^.{4}"
  if [ $? -eq 0 ] ; then
    echo "$arg"
  fi
done
exit 0

