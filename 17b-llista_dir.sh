#! /bin/bash 
# @edt ASIX-M01 
# 16 de Març 2023 
# Pablo Ruz Muñoz 
# Programa que comprova si el argument és 
# un directori i si ho és mostra la llista 
# del directori introduit 
# Utilització: prog dir 
# ---------------------------------------------------------------------- 

ERR_ARGS=1
ERR_TYPE_ARG=2

#1) Validació del nombre d'arguments

if [ $# -ne 1 ];then
  echo "ERROR: El nombre d'arguments és incorrecte"
  echo "USAGE: $0 dir"
  exit $ERR_ARGS
fi

#2) Validació de si és un directori

dir=$1

if ! [ -d $dir ];then
  echo "ERROR: $dir no és un directori"
  echo "USAGE: $0 dir"
  exit $ERR_TYPE_ARG
fi

#3) xixa

llista=$(ls $dir)
contador=1

for elem in $llista
do
  echo "$contador: $elem"
  ((contador++))
done
exit 0
