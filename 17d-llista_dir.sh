#! /bin/bash 
# @edt ASIX-M01 
# 16 de Març 2023 
# Pablo Ruz Muñoz 
# Programa que comprova si el argument és 
# un directori i si ho és mostra la llista 
# del directori introduit 
# Utilització: prog dir 
# ---------------------------------------------------------------------- 

ERR_ARGS=1

#1) Validació del nombre d'arguments

if [ $# -lt 1 ];then
  echo "ERROR: El nombre d'arguments és incorrecte"
  echo "USAGE: $0 dir"
  exit $ERR_ARGS
fi

#2) xixa

for dir in $*
do
  if ! [ -d $dir ];then
    echo "ERROR: $dir no és un directori!" >> 1/&2	
  else
    llista=$(ls $dir)
    echo "Analisi de $dir:"
    for elem in $llista
    do
      if [ -h $dir/$elem ];then
        echo -e "\t$elem és un link"
      elif [ -f $dir/$elem ];then
        echo -e "\t$elem és un regular file"
      elif [ -d $dir/$elem ];then
        echo -e "\t$elem és un directori"
      else
        echo -e "\t$elem és una altra cosa"
      fi
    done
  fi
done
exit 0
