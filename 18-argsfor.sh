#! /bin/bash
# @edt ASIX-M01
# 22 de març de 2023
# Pablo Ruz Muñoz
# Descripció: Programa que llegeix arguments i determina si són opcions o arguments
# Utilització: prog elem...

# 1) Validar arguments

if [ $# -lt 1 ];
then
  echo "ERROR: El nombre d'arguments és incorrecte"
  echo "USAGE: $0 elem..."
  exit 1
fi

# 2) xixa

opcions=""
arguments=""

for elem in $*
do
  case $elem in
  "-a"|"-b"|"-c"|"-d")
    opcions="$opcions $elem "
    ;;
  *)
    arguments="$arguments $elem "
    ;;
  esac
done

echo "Les opcions són: $opcions"
echo "Els arguments són: $arguments"
